terraform {
  backend "s3" {}
  required_version = "~> 0.13"
}

provider "aws" {
  region  = var.aws_region
  version = "= 3.11.0"
  profile = var.profile
}

//noinspection MissingModule
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "vpc-${var.environment}"
  cidr = var.cidr

  azs                 = var.azs

  public_subnets      = var.public_subnets
  private_subnets     = var.private_subnets

  enable_nat_gateway           = true
  single_nat_gateway           = true
  one_nat_gateway_per_az       = false
  enable_dns_hostnames         = true
  enable_dns_support           = true
  public_subnet_tags = {
    "kubernetes.io/role/elb" = 1
  }
  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = 1
  }
  tags = {
    Terraform                                                         = "true"
    Environment                                                       = var.environment
    "kubernetes.io/cluster/eks-${var.environment}-cluster"            = "shared"
  }
}


resource "aws_flow_log" "flow_log" {
  log_destination      = aws_s3_bucket.flow_log.arn
  log_destination_type = "s3"
  traffic_type         = "REJECT"
  vpc_id               = module.vpc.vpc_id
}

resource "aws_s3_bucket" "flow_log" {
  bucket = "vpc-flow-log-${var.environment}"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

}

resource "aws_s3_bucket_public_access_block" "flow_log" {
  bucket                  = aws_s3_bucket.flow_log.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}