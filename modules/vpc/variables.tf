variable "aws_region" {
  description = "The AWS region to deploy to (e.g. us-east-1)"
  type        = string
}

variable "profile" {
  description = "The AWS profile"
  type        = string
}

variable "environment" {
  description = "Environment"
  type        = string
}


variable "cidr" {
  description = "vpc cidr"
  type = string
}

variable "azs" {
  description = "vpc azs"
  type = list
}

variable "public_subnets" {
  description = "public subnets cidr"
  type = list
}

variable "private_subnets" {
  description = "private subnets cidr"
  type = list
}
