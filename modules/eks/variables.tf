variable "aws_region" {
  description = "The AWS region to deploy to (e.g. us-east-1)"
  type        = string
}

variable "profile" {
  description = "The AWS profile"
  type        = string
}

variable "environment" {
  description = "Environment"
  type        = string
}

variable "vpc_id" {
  description = "VPC id"
  type        = string
}
variable "fargate_enable" {
  type = bool
}

variable "k8s_autosaler_service_account_name" {
  type = string
}

