output "cluster_id" {
  value = module.eks.cluster_id
}

output "this_iam_role_arn" {
  value = module.iam_assumable_role_admin.this_iam_role_arn
}