terraform {
  source = "git::git@gitlab.com:polarsquad/gitlab/gitlab-runner-eks.git//modules/helm?ref=v1.0.21"
}

dependency "eks" {
  config_path = "../eks"
}

include {
  path = find_in_parent_folders()
}

locals {
  common-vars  = yamldecode(file("${find_in_parent_folders("common-vars.yaml")}"))
}

inputs = {
  aws_region  = local.common-vars.aws_region
  profile     = local.common-vars.profile
  environment = local.common-vars.environment
  k8s_autosaler_service_account_name = local.common-vars.k8s_autosaler_service_account_name
  cluster_id  = dependency.eks.outputs.cluster_id
  role-arn    = dependency.eks.outputs.this_iam_role_arn
  
}
