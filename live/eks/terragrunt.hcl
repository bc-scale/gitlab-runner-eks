terraform {
  source = "git::git@gitlab.com:polarsquad/gitlab/gitlab-runner-eks.git//modules/eks?ref=v1.0.20"
}

dependency "vpc" {
  config_path = "../vpc"
}

include {
  path = find_in_parent_folders()
}

locals {
  common-vars  = yamldecode(file("${find_in_parent_folders("common-vars.yaml")}"))
}

inputs = {
  aws_region  = local.common-vars.aws_region
  profile     = local.common-vars.profile
  environment = local.common-vars.environment
  k8s_autosaler_service_account_name = local.common-vars.k8s_autosaler_service_account_name
  vpc_id      = dependency.vpc.outputs.vpc_id
  fargate_enable = false
  
}
